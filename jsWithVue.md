<!--
 Projet AgriBIoT - Centre de Recherche INRIA Rennes
 INRIA Startup Studio - 2020-2021
 Copyright : AgriBIoT (c) 2020-2021
 Module Name : JS integration docs

 Author : Aiswarya Kaitheri-Kandoth (aiswarya.kaitheri-kandoth@irisa.fr)

 Encadré par : Simon Tropée (simon.tropee@inria.fr)
 Responsable du projet : Oscar Roberto Bastos (roberto.bastos@inria.fr)

 Licence : CC0 1.0 see : https://creativecommons.org/publicdomain/zero/1.0/
-->

## Django with Vue

Pour les developers que veulent pas faire des designs le bibliothèque [Vuetify](https://vuetifyjs.com/en/) est un bon moyen.

Pour la table il y a un simple bibliothèque, vue-good-table.

Notez que dans la forme si on veut un champ comme 'selectionnez une plante', voici un moyen :

>       <option disabled value="">selectionnez une plante</option>
