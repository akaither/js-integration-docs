<!--
 Projet AgriBIoT - Centre de Recherche INRIA Rennes
 INRIA Startup Studio - 2020-2021
 Copyright : AgriBIoT (c) 2020-2021
 Module Name : JS integration docs

 Author : Aiswarya Kaitheri-Kandoth (aiswarya.kaitheri-kandoth@irisa.fr)

 Encadré par : Simon Tropée (simon.tropee@inria.fr)
 Responsable du projet : Oscar Roberto Bastos (roberto.bastos@inria.fr)

 Licence : CC0 1.0 see : https://creativecommons.org/publicdomain/zero/1.0/
-->
# Intégrer JS frameworks avec Django - notes

### Les deux possibilités

##### 1. L’architecture **client-first**

Elle sépare clairement et proprement le front-end et le backend. Il faut prévoir dès le départ de mettre en œuvre cette architecture.

Le parcours typique de cette architecture commence par le front-end. On choisit un framework JavaScript et configure le front-end avec les outils recommandés par ce framework (quelque chose comme create-react-app ou vue create).

Le backend est un projet Django complètement séparé et autonome, où Django est principalement une API pour la base de données. Les projets client-first font presque toujours un usage intensif de Django Rest Framework.

Les principaux inconvénients sont les suivants :  
> Oblige à réimplémenter de nombreuses fonctionnalités de Django dans le front-end.  
> Les tâches simples sont plus difficiles.  

##### 2. L’architecture **hybrid**

Le concept clé est le suivant : dans une architecture hybride, plutôt que de choisir entre l'architecture client-first ou basé sur django au niveau du projet, on le choisit au niveau de la page.

Pour certaines pages (login, contenu statique, simples UI etc.), on laisse le Django porter la charge. Pour d'autres pages où on a besoin d'un front-end complexe, on utilise les principes du client-first. Et pour tout ce qui est entre les deux, on utilise un toolchain qui nous permet de combiner Django avec une front-end codebase claire.

### Integration - pratique

##### Où mettre des choses

Tous les fichiers sources du front-end dans un sous-dossier à la racine du projet Django (exemple : ./assets/).

Pour tous les sorties de bundler (Django ne fonctionnera qu'avec les sorties générées par le bundler), un dossier ./static/ au niveau de la racine. Pour que le système de fichiers statique de Django puisse les trouver.

##### Setup webpack dans le projet Django

Initialiser un nouveau projet npm dans la racine du projet Django et install webpack  
>npm init -y  
>npm install webpack webpack-cli --save-dev  

Met le dossier node_modules dans .gitignore. Cet un dossier pour où des dependence de bibliothèque JS vas atterrir.

Crée ./assets/index.js pour tester la configuration

Ajouter config fichier pour webpack, webpack.config.js, dans la racine du projet.

Finalement, ajouter une cible de script npm pour exécuter webpack. Dans package.json, ajoutez la ligne suivante à la clé des scripts :  
>"dev": "webpack --mode development --watch"  

--watch se reconstruira automatiquement lors des changements de fichiers.

Pour exécuter le script webpack, utilise la commande suivant :  
>npm run dev  

./static/index-bundle.js doit créer.

##### Connecter webpack à Django

Ajouter un template de base, hello_webpack.html  
Ajouter urlpatterns et des liens nécessaire pour lire des fichiers static et templates dans settings.py  

##### Travailler avec des bibliothèques JavaScript externes en utilisant NPM

Exemple de bibliothèque externe, lodash :

Installer lodash en utilisant npm

Mise à jour le ./assets/index.js et re-faire (NE OUBLIEZ PAS EXÉCUTER CETTE COMMANDE CHAQUE FOIS ou continuez à l’exécuter dans une autre fenêtre de terminal pour qu’il se mette à jour à chaque fois) :  
>npm run dev
