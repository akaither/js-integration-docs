<!--
 Projet AgriBIoT - Centre de Recherche INRIA Rennes
 INRIA Startup Studio - 2020-2021
 Copyright : AgriBIoT (c) 2020-2021
 Module Name : JS integration docs

 Author : Aiswarya Kaitheri-Kandoth (aiswarya.kaitheri-kandoth@irisa.fr)

 Encadré par : Simon Tropée (simon.tropee@inria.fr)
 Responsable du projet : Oscar Roberto Bastos (roberto.bastos@inria.fr)

 Licence : CC0 1.0 see : https://creativecommons.org/publicdomain/zero/1.0/
-->

##### React + Babel

Installer Babel (@babel/core), loader webpack pour Babel (babel-loader) et deux presets env-preset et react-preset.  
>npm install --save-dev babel-loader @babel/core @babel/preset-env @babel/preset-react

preset-env aide pour la compatibilité avec des Browser.

On doit indiquer webpack à utiliser le preset pour traiter nos dossiers. Pour cela, ajouter quelque ligne dans webpack.config.js. Ceux la dire, utiliser les preset env et react de Babel pour compiler tous les fichiers .js et .jsx qui ne se trouvent pas dans le répertoire node_modules.

Ajouter la ligne suivant dans Django template avant d’ajouter le bundle :  
>        <div id="root" />

Installer React et ReactDOM :  
>npm install --save react react-dom 

Mise à jour d’index.js en version React.

##### Retour d'integration React + Django

Note : L'extention 'React developer tool' est très utile pendent le développement.

Django REST est meilleur moyen pour transférer les data entre frontend et backend. 

Les étapes :

1. Ajouter Django template dans template/petiteferme/ avec une div id "js-framework-home".  

2. Dans le même template ajouter static script. Notez que le script doit à la fin avant de la clôture de body. Par exemple :

>       {% extends "petiteferme/base.html" %}
>       {% block content %}
>       <!-- div that the JavaScript framework will render in -->
>       <div id="js-framework-home"></div>
>       <!-- bundle file with our JavaScript application logic -->
>       <script src="{% static 'index-bundle.js' %}"></script>
>       {% endblock content %}  

c’est comme ça on passe le js bundle à Django

3. On monte l'appli React en utilisant le div id "js-framework-home" du template dans index.js (codebase de JS)

4. Transférer les données de l'API dans frontend : REST recommande la bibliothèque JavaScript Core API qui est prise en charge de manière native.

Il faut initialiser le client de core API avant passer dans le component React. Et sauvegarder les data de coreapi en utilisant state variable de React.

Pour comprendre la bonne pratique en React : <https://reactjs.org/docs/thinking-in-react.html#step-1-break-the-ui-into-a-component-hierarchy>

## Ajouter css

Il faut webpack loader pour traiter les fichiers css. Installer css-loader et style-loader:

>       npm install style-loader css-loader --save-dev

Et ajouter dans la config de webpack:

> {
>      test: /\.css$/,
>      exclude: /node_modules/,
>      use:['style-loader','css-loader'],
> },

## Des commentaires générales 

- Le concept 'state' dans react doit utiliser pour éviter des actualisation pendent le mis à jour de data.
- Il faut créer un mock-up avant de faire un page. Laquelle peut être très utils pour decider des components et le data-flow dans React.
